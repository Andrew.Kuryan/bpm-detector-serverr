plugins {
    id("org.jetbrains.kotlin.jvm") version "1.5.0"
    application
}

repositories {
    mavenCentral()
    maven("https://dl.bintray.com/kotlin/exposed/")
}

val coroutines_version = "1.5.0"
val junit_version = "5.7.0"

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-jvm:$coroutines_version")

    implementation("org.reduxkotlin:redux-kotlin-threadsafe-jvm:0.5.5")

    implementation("org.apache.commons:commons-math3:3.6.1")
    implementation("com.googlecode.soundlibs:mp3spi:1.9.5.4")
    implementation("com.github.salomonbrys.kotson:kotson:2.5.0")

    implementation("ch.qos.logback:logback-classic:1.2.3")

    implementation("io.ktor:ktor-server-core:1.5.4")
    implementation("io.ktor:ktor-server-cio:1.5.4")
    implementation("io.ktor:ktor-gson:1.5.4")
    implementation("io.ktor:ktor-websockets:1.5.4")

    implementation("org.jetbrains.exposed", "exposed-core", "0.24.1")
    implementation("org.jetbrains.exposed", "exposed-jdbc", "0.24.1")
    implementation("com.h2database:h2:1.4.200")

    testImplementation(platform("org.junit:junit-bom:$junit_version"))
    testImplementation("org.junit.jupiter:junit-jupiter:$junit_version")
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}

application {
    mainClass.set("com.appspot.magtech.AppKt")
}

tasks.named<Jar>("jar") {
    exclude("**/resources/")
    manifest {
        attributes("Main-Class" to "com.appspot.magtech.AppKt")
    }
    configurations["compileClasspath"].forEach { file: File ->
        from(zipTree(file.absoluteFile))
    }
    archiveBaseName.set("detector")
    duplicatesStrategy = DuplicatesStrategy.INCLUDE
}