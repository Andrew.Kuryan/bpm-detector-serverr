package com.appspot.magtech

import javax.sound.sampled.AudioFormat

interface AudioFile {

    val name: String
    val numFrames: Long

    val decodedFormat: AudioFormat

    val amplitudesLeft: DoubleArray
    val amplitudesRight: DoubleArray
}