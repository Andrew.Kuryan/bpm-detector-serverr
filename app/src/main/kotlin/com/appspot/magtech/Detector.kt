package com.appspot.magtech

import com.appspot.magtech.bpm.BlockNotesDetector
import com.appspot.magtech.bpm.finishCount
import com.appspot.magtech.bpm.nextBlock
import com.appspot.magtech.logic.STFT
import com.appspot.magtech.logic.interpolateSource
import com.appspot.magtech.notes.Note
import com.appspot.magtech.notes.calcNotes
import kotlin.math.ceil
import kotlin.math.roundToInt

data class DetectorConfig(
    val maxWindowSize: Int = 32768,
    val minWindowSize: Int = 512,
)

val intervals = listOf(
    listOf(
        9 until 21,
        21 until 33,
        33 until 45,
        45 until 57,
        57 until 69,
        69 until 81,
        81 until 93,
        93 until 105,
    ),
    listOf(
        9 until 33,
        21 until 45,
        33 until 57,
        45 until 69,
        57 until 81,
        69 until 93,
        81 until 105,
    ),
    listOf(
        9 until 45,
        21 until 57,
        33 until 69,
        45 until 81,
        57 until 93,
        69 until 105,
    ),
    listOf(
        9 until 57,
        21 until 69,
        33 until 81,
        45 until 93,
        57 until 105,
    ),
    listOf(
        9 until 69,
        21 until 81,
        33 until 93,
        45 until 105,
    ),
    listOf(
        9 until 81,
        21 until 93,
        33 until 105,
    ),
    listOf(
        9 until 93,
        21 until 105,
    ),
)

val flatIntervals = intervals.flatten()

data class ProcessFileResult(
    val bpmMin: Double,
    val bpmDev: Double,
    val bpmMedian: Double,
    val tonality: Tonality,
    val notePercents: Map<Int, Double>
)

suspend fun processFile(
    file: AudioFile,
    config: DetectorConfig,
    updateProgress: suspend (Double) -> Unit
): ProcessFileResult {
    val sourceLeft = file.amplitudesLeft

    val minFreqStep = file.decodedFormat.sampleRate.toDouble() / config.maxWindowSize

    val sourceSizeInWindows = ceil(sourceLeft.size.toDouble() / config.maxWindowSize).toInt()
    val normalizedSource: DoubleArray = sourceLeft +
            DoubleArray(sourceSizeInWindows * config.maxWindowSize - sourceLeft.size) +
            DoubleArray(config.maxWindowSize / 2)

    val detectors = mutableMapOf<IntRange, BlockNotesDetector>()
    flatIntervals.forEach {
        detectors[it] = BlockNotesDetector(file.decodedFormat.sampleRate, config.minWindowSize)
    }

    val halfMaxWinSize = config.maxWindowSize / 2
    val halfMinWinSize = config.minWindowSize / 2
    val noteDurations = Note.bases.map { it to 0 }.toMap().toMutableMap()

    val totalSteps = ((normalizedSource.size - halfMaxWinSize) - halfMaxWinSize)
    var prevPercent = 0

    for (i in halfMaxWinSize..(normalizedSource.size - halfMaxWinSize) step config.minWindowSize) {
        val dpfRes = STFT(normalizedSource, i, halfMaxWinSize)
            .take(7040)

        val dpfSmallRes = STFT(normalizedSource, i, halfMinWinSize)
            .take(110)

        val interpolated = interpolateSource(dpfSmallRes, 7040)
        val resultMap = dpfRes.mapIndexed { index, d -> d * interpolated[index] }

        val notes = calcNotes(resultMap, minFreqStep).withIndex().toList()

        val resultMapAverage = resultMap.average()

        val soil = resultMapAverage * 26.0

        intervals.forEachIndexed { _, intervals ->
            intervals.forEach { interval ->
                val octaveNoteIndexes =
                    notes.slice(interval).filter { it.value > soil }.map { it.index }
                detectors.getValue(interval).nextBlock(octaveNoteIndexes)
                octaveNoteIndexes.forEach {
                    val noteOctave = it / 12
                    val noteIndex = it - noteOctave * 12
                    val note = Note.bases[noteIndex]
                    noteDurations[note] = noteDurations.getValue(note) + 1
                }
            }
        }

        val newPercent = (i.toDouble() / totalSteps * 100.0).roundToInt()
        if (newPercent > prevPercent) {
            prevPercent = newPercent
            updateProgress(newPercent.toDouble() / 100.0)
        }
    }

    val devRes = intervals.map { intervals ->
        intervals.map {
            val res = detectors.getValue(it).finishCount()
            res
        }.minByOrNull { it.bpm }!!
    }.minByOrNull { it.bpm }!!

    val devRes1 = flatIntervals.map { detectors.getValue(it).finishCount() }.minByOrNull { it.deviation }!!
    val devRes2 =
        flatIntervals.map { detectors.getValue(it).finishCount() }.sortedBy { it.bpm }[flatIntervals.size / 2]

    val tonality = pickUpTonality(noteDurations)

    val filteredSizeMap = devRes.sizeMap.filter {
        it.key == devRes.index ||
                it.key == devRes.index * 2 ||
                it.key == devRes.index * 4 ||
                it.key == devRes.index * 8
    }
    val notesCount = filteredSizeMap.entries.sumBy { it.value }
    val notePercents = filteredSizeMap.mapKeys {
        when (it.key) {
            devRes.index -> 8
            devRes.index * 2 -> 4
            devRes.index * 4 -> 2
            devRes.index * 8 -> 1
            else -> 0
        }
    }.mapValues {
        it.value.toDouble() / notesCount
    }

    return ProcessFileResult(devRes.bpm, devRes1.bpm, devRes2.bpm, tonality, notePercents)
}