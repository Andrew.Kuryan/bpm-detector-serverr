package com.appspot.magtech

import com.appspot.magtech.logic.correlationCoefficient
import com.appspot.magtech.notes.Note

val majorKSProfile = listOf(6.35, 2.23, 3.48, 2.33, 4.38, 4.09, 2.52, 5.19, 2.39, 3.66, 2.29, 2.88)
val minorKSProfile = listOf(6.33, 2.68, 3.52, 5.38, 2.6, 3.53, 2.54, 4.75, 3.98, 2.69, 3.34, 3.17)

enum class Mode { Major, Minor }
data class Tonality(val mode: Mode, val note: Note) {

    override fun toString(): String {
        return "${note.displayName} $mode"
    }
}

fun pickUpTonality(durationMap: Map<Note, Int>): Tonality {
    val sequence = listOf(
        Note.C, Note.`C#`, Note.D, Note.`D#`, Note.E, Note.F, Note.`F#`, Note.G, Note.`G#`, Note.A, Note.`A#`, Note.B
    )

    data class TonalityVariant(val tonality: Tonality, val coefficient: Double)

    val variants = mapOf(Mode.Major to majorKSProfile, Mode.Minor to minorKSProfile).entries.map { (mode, profile) ->
        sequence.map { tone ->
            val toneSequence =
                with(sequence.indexOf(tone)) { sequence.subList(this, sequence.size) + sequence.subList(0, this) }
            TonalityVariant(
                Tonality(mode, tone),
                correlationCoefficient(profile, toneSequence.map { durationMap.getValue(it).toDouble() })
            )
        }
    }.flatten()
    return variants.maxByOrNull { it.coefficient }!!.tonality
}