package com.appspot.magtech

import java.nio.file.Path
import javax.sound.sampled.AudioFormat
import javax.sound.sampled.AudioSystem
import kotlin.math.abs

fun Byte.toUsignByte(): Int =
    when {
        this < 0 -> abs(this.toInt()) + 127
        else -> this.toInt()
    }

fun Pair<Byte, Byte>.fromPcmSigned(): Int {
    val start = (second.toUsignByte() shl 8) or first.toUsignByte()
    return if (start < 32768) {
        start
    } else {
        32768 - start
    }
}

class MP3AudioFile(path: Path) : AudioFile {

    private val audioIn = AudioSystem.getAudioInputStream(path.toFile())
    private val baseFormat = audioIn.format

    override val name = path.fileName.toString()
    override val decodedFormat = AudioFormat(
        AudioFormat.Encoding.PCM_SIGNED,
        baseFormat.sampleRate,
        16,
        2,
        2 * 2,
        baseFormat.sampleRate,
        false
    )

    private val decodedIn = AudioSystem.getAudioInputStream(decodedFormat, audioIn)
    private val bytes = decodedIn.readAllBytes()

    private val amplitudes = bytes.toAmplitudesList()
    override val amplitudesLeft = amplitudes.chunked(2).map { it[0] }.toDoubleArray()
    override val amplitudesRight = amplitudes.chunked(2).map { it[1] }.toDoubleArray()

    override val numFrames = (bytes.size / 4).toLong()

    private fun ByteArray.toAmplitudesList(): List<Double> {
        val amplitudes = mutableListOf<Double>()
        for (i in indices step 2) {
            amplitudes.add((this[i] to this[i + 1]).fromPcmSigned().toDouble())
        }
        return amplitudes
    }
}