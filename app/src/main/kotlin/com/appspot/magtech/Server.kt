package com.appspot.magtech

import com.appspot.magtech.api.db.TracksApi
import com.appspot.magtech.api.parsers.serializeAnalysisListUpdate
import com.appspot.magtech.api.parsers.serializeTrackAnalysis
import com.appspot.magtech.application.ReduxContext
import com.appspot.magtech.application.analysis.AnalysisAction
import com.appspot.magtech.application.analysis.AnalysisTask
import com.appspot.magtech.application.analysis.HashGenerator
import com.appspot.magtech.application.configureStore
import com.appspot.magtech.entities.NotAvailableAnalysis
import com.appspot.magtech.entities.ProcessingAnalysis
import com.appspot.magtech.entities.computeDifference
import com.appspot.magtech.extensions.flow.zipLast
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.http.*
import io.ktor.http.cio.websocket.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.cio.*
import io.ktor.server.engine.*
import io.ktor.websocket.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.withContext

data class NewTrackRequest(val id: Int, val localPath: String)

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
fun launchServer() {
    val storagePath = "downloads/"
    val tracksApi = TracksApi(storagePath)
    val (states, actions) = configureStore(tracksApi)

    actions.trySendBlocking(AnalysisAction.LoadAnalysis)

    embeddedServer(CIO, 3355) {
        install(ContentNegotiation) {
            gson()
        }
        install(WebSockets)
        println("""{ "status": "working" }""")

        routing {
            get("/status") {
                call.respondText("""{ "status": "working" }""", ContentType.Application.Json)
            }
            put("/new-track") {
                val body = call.receive<NewTrackRequest>()
                withContext(ReduxContext) {
                    actions.send(
                        AnalysisAction.TaskAdded(
                            body.id,
                            body.localPath,
                            HashGenerator.nextValue
                        )
                    )
                }
                call.respondText("""{ "status": "OK" }""", ContentType.Application.Json)
            }
            get("/analysis/{trackId}") {
                val trackId = call.parameters["trackId"]?.toIntOrNull() ?: -1
                val analysis = states
                    .map { state -> state.analysisState.processingTracks.filter { it.trackId == trackId } }
                    .first()
                    .maxByOrNull { it.hash }
                    .let {
                        when (it) {
                            null, is AnalysisTask.Ready, is AnalysisTask.Failed -> NotAvailableAnalysis
                            is AnalysisTask.Processing -> ProcessingAnalysis(it.processingPercent)
                            is AnalysisTask.Finished -> it.analysis
                        }
                    }
                call.respondText(serializeTrackAnalysis(analysis), ContentType.Application.Json)
            }
            delete("/analysis/{trackId}") {
                val trackId = call.parameters["trackId"]?.toIntOrNull() ?: -1
                withContext(ReduxContext) { actions.send(AnalysisAction.RemoveAnalysis(trackId)) }
                call.respondText("""{ "status": "OK" }""", ContentType.Application.Json)
            }
            get("/analysis") {
                val analysisList = states
                    .map { it.analysisState.processingTracks }
                    .first()
                    .groupBy { it.trackId }
                    .mapValues { (_, value) -> value.maxByOrNull { it.hash }!! }
                    .values
                    .map {
                        it.trackId to when (it) {
                            is AnalysisTask.Ready, is AnalysisTask.Failed -> NotAvailableAnalysis
                            is AnalysisTask.Processing -> ProcessingAnalysis(it.processingPercent)
                            is AnalysisTask.Finished -> it.analysis
                        }
                    }
                call.respondText("""[${
                    analysisList.joinToString(",") {
                        """{
                             |"trackId": ${it.first};
                             |"analysis": ${serializeTrackAnalysis(it.second)}
                          |}""".trimMargin()
                    }
                }]""")
            }

            webSocket("/updates") {
                states.zipLast(2).mapNotNull { (newState, prevState) ->
                    if (prevState != null && newState != null) {
                        computeDifference(
                            prevState.analysisState.processingTracks,
                            newState.analysisState.processingTracks,
                        )
                    } else null
                }.collect {
                    outgoing.send(Frame.Text(serializeAnalysisListUpdate(it)))
                }
            }
        }
    }.start(wait = true)
}