package com.appspot.magtech

import com.appspot.magtech.notes.Note
import kotlinx.coroutines.runBlocking
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.math.abs

val pianoTests = mapOf(
    Paths.get("src/test/resources/piano/Star Wars - Duel of the Fates.mp3") to 128.57, // 129 0,33 | 130 1,11 | 130 1,11
    Paths.get("src/test/resources/piano/Star Wars - The Force Theme.mp3") to 80.0, // 81 1,25 | 80 0,00 | 80 0,00
    Paths.get("src/test/resources/piano/Star Wars - General Grievous Suite.mp3") to 101.4, // 123 21,3 | 185 82,45 | 135 33,14
    Paths.get("src/test/resources/piano/Pirates of the Caribbean - Davy Jones.mp3") to 110.2, // 112 1,63 | 115 4,35 | 113 2,54
    Paths.get("src/test/resources/piano/The Lord of the Rings - Main Theme.mp3") to 80.9, // 152 87,88 | 80 1,125 | 138 70,58
    Paths.get("src/test/resources/piano/Star Wars - Cantina Band.mp3") to 240.0, // 117 51,25 | 118 50,83 | 119 50,42
    Paths.get("src/test/resources/piano/Star Wars - Across the Stars.mp3") to 115.7, // 74 36,04 | 75 34,57 | 62 46,41
    Paths.get("src/test/resources/piano/Game of Thrones - Main Theme.mp3") to 84.9, // 172 102,6 | 172 102,6 | 84 1,06
    Paths.get("src/test/resources/piano/Requiem for a Dream - Lux Aeterna.mp3") to 75.0, // 76 1,33 | 150 100,00 | 75 0,00
    // 303,61 | 377,035 | 205,26
    Paths.get("src/test/resources/piano/Pirates of the Caribbean - He's a Pirate.mp3") to 180.0, // 185 2,78 | 118 34,44 | 120 33,33
)

val pianoTonality = mapOf(
    "Star Wars - The Force Theme.mp3" to Tonality(Mode.Minor, Note.C),
    "Star Wars - General Grievous Suite.mp3" to Tonality(Mode.Minor, Note.D),
    "Pirates of the Caribbean - Davy Jones.mp3" to Tonality(Mode.Minor, Note.D),
    "Star Wars - Duel of the Fates.mp3" to Tonality(Mode.Minor, Note.E),
    "The Lord of the Rings - Main Theme.mp3" to Tonality(Mode.Major, Note.D),
    "Star Wars - Cantina Band.mp3" to Tonality(Mode.Minor, Note.D),
    "Star Wars - Across the Stars.mp3" to Tonality(Mode.Minor, Note.D),
    "Game of Thrones - Main Theme.mp3" to Tonality(Mode.Minor, Note.C),
    "Requiem for a Dream - Lux Aeterna.mp3" to Tonality(Mode.Minor, Note.G),
    "Pirates of the Caribbean - He's a Pirate.mp3" to Tonality(Mode.Minor, Note.D),
)

val syntheticTests = mapOf(
    Paths.get("src/test/resources/synthetic/Star Wars - Luke Skywalker Theme | EPIC MANDALORIAN VERSION.mp3") to 77.42, // 78 0,75 | 79 2,04 | 109 40,79
    Paths.get("src/test/resources/synthetic/Star Wars - General Grievous Epic Theme.mp3") to 90.0, // 185 105,56 | 156 73,33 | 87 3,33
    Paths.get("src/test/resources/synthetic/Star Wars - Grand Admiral Thrawn Theme.mp3") to 80.0, // 108 35,00 | 80 0,00 | 106 32,5
    Paths.get("src/test/resources/synthetic/Lord of The Rings - Rohan Theme.mp3") to 67.92, // 123 81,095 | 148 117,9 | 96 41,34
    Paths.get("src/test/resources/synthetic/Transformers - Arrival To Earth x Hearts of Courage.mp3") to 70.59, // 144 104,00 | 143 102,58 | 121 71,41
    Paths.get("src/test/resources/synthetic/Lord of The Rings - The Shire Theme.mp3") to 83.3, // 172 106,48 | 113 35,65 | ? ?
    Paths.get("src/test/resources/synthetic/Up is Down & Battle of The Heroes | EPIC VERSION.mp3") to 133.3, // 129 3,23 | 130 2,48 | 88 33,98
    Paths.get("src/test/resources/synthetic/Batman Epic Theme.mp3") to 112.15, // 96 14,4 | 97 13,51 | 147 30,67
)

fun testTracks(tests: Map<Path, Double>) {
    val config = DetectorConfig()

    val derivations = tests.map { (path, bpm) ->
        val audioFile = MP3AudioFile(path)
        val (tempoMin, tempoDev, tempoMedian) = runBlocking {
            processFile(audioFile, config) {}
        }
        println(tempoMin)
        println(tempoDev)
        println(tempoMedian)

        Triple(
            (abs(tempoMin - bpm) / bpm) * 100,
            (abs(tempoDev - bpm) / bpm) * 100,
            (abs(tempoMedian - bpm) / bpm) * 100,
        )
    }
    println(derivations.map { it.first })
    println(derivations.map { it.second })
    println(derivations.map { it.third })
    println("Average min: ${derivations.map { it.first }.average()}")
    println("Average dev: ${derivations.map { it.second }.average()}")
    println("Average median: ${derivations.map { it.third }.average()}")
}