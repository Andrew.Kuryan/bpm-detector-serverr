package com.appspot.magtech.api.db

import org.jetbrains.exposed.sql.Table

object TrackAnalysisTable : Table("track_analysis") {
    val trackId = integer("track_id")
    val bpm = integer("bpm").nullable()
    val key = varchar("key", 64).nullable()
    val x8Percent = double("x8_percent").nullable()
    val x4Percent = double("x4_percent").nullable()
    val x2Percent = double("x2_percent").nullable()
    val x1Percent = double("x1_percent").nullable()

    override val primaryKey = PrimaryKey(trackId, name = "pk_track_analysis")
}