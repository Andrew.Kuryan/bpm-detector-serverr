package com.appspot.magtech.api.db

import com.appspot.magtech.entities.FulfilledAnalysis
import com.appspot.magtech.entities.NotAvailableAnalysis
import com.appspot.magtech.entities.TrackAnalysis
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync
import org.jetbrains.exposed.sql.transactions.transaction
import java.sql.Connection

class TracksApi(storagePath: String) {

    // ;AUTO_SERVER=TRUE;AUTO_SERVER_PORT=8043;AUTO_RECONNECT=TRUE;DB_CLOSE_ON_EXIT=FALSE;DB_CLOSE_DELAY=-1;
    private val db = Database.connect(
        url = "jdbc:h2:./${storagePath}detector",
        driver = "org.h2.Driver"
    )

    init {
        transaction(Connection.TRANSACTION_SERIALIZABLE, 2, db) {
            SchemaUtils.create(TrackAnalysisTable)
        }
    }

    suspend fun getAllTrackAnalysis(): List<Pair<Int, FulfilledAnalysis>> {
        return suspendedTransactionAsync(Dispatchers.IO, db, Connection.TRANSACTION_SERIALIZABLE) {
            TrackAnalysisTable.selectAll().map { it[TrackAnalysisTable.trackId] to parseTrackAnalysis(it) }
        }.await()
    }

    suspend fun getTrackAnalysis(trackId: Int): TrackAnalysis {
        return suspendedTransactionAsync(Dispatchers.IO, db, Connection.TRANSACTION_SERIALIZABLE) {
            TrackAnalysisTable.select { TrackAnalysisTable.trackId eq trackId }
                .firstOrNull()?.let { parseTrackAnalysis(it) } ?: NotAvailableAnalysis
        }.await()
    }

    suspend fun removeTrackAnalysis(trackId: Int) {
        suspendedTransactionAsync(Dispatchers.IO, db, Connection.TRANSACTION_SERIALIZABLE) {
            TrackAnalysisTable.deleteWhere { TrackAnalysisTable.trackId eq trackId }
        }.await()
    }

    suspend fun createTrackAnalysis(trackId: Int, analysis: FulfilledAnalysis) {
        suspendedTransactionAsync(Dispatchers.IO, db, Connection.TRANSACTION_SERIALIZABLE) {
            TrackAnalysisTable.insert {
                it[TrackAnalysisTable.trackId] = trackId
                it[bpm] = analysis.bpm
                it[key] = analysis.tonality.toString()
                it[x1Percent] = analysis.notePercents.getValue(1)
                it[x2Percent] = analysis.notePercents.getValue(2)
                it[x4Percent] = analysis.notePercents.getValue(4)
                it[x8Percent] = analysis.notePercents.getValue(8)
            }
        }.await()
    }

    private fun parseTrackAnalysis(row: ResultRow): FulfilledAnalysis {
        return FulfilledAnalysis(
            bpm = row[TrackAnalysisTable.bpm]!!,
            key = row[TrackAnalysisTable.key]!!,
            notePercents = mapOf(
                1 to row[TrackAnalysisTable.x1Percent]!!,
                2 to row[TrackAnalysisTable.x2Percent]!!,
                4 to row[TrackAnalysisTable.x4Percent]!!,
                8 to row[TrackAnalysisTable.x8Percent]!!,
            )
        )
    }
}