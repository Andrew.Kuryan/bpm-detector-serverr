package com.appspot.magtech.api.parsers

import com.appspot.magtech.application.analysis.AnalysisTask
import com.appspot.magtech.entities.*
import java.util.*

fun serializeTrackAnalysis(analysis: TrackAnalysis): String {
    val type = when (analysis) {
        is NotAvailableAnalysis -> "not_available"
        is ProcessingAnalysis -> "processing"
        is FulfilledAnalysis -> "fulfilled"
    }
    val data = when (analysis) {
        is NotAvailableAnalysis -> """{}"""
        is ProcessingAnalysis -> """{ "percent": ${String.format(Locale.US, "%.2f", analysis.percent)} }"""
        is FulfilledAnalysis -> serializeFulfilledAnalysis(analysis)
    }
    return """{
                |"type": "$type";
                |"data": $data
             |}""".trimMargin()
}

fun serializeFulfilledAnalysis(analysis: FulfilledAnalysis): String {
    return """{
                |"bpm": ${analysis.bpm};
                |"key": "${analysis.tonality}";
                |"x1Percent": ${String.format(Locale.US, "%.2f", analysis.notePercents.getValue(1))};
                |"x2Percent": ${String.format(Locale.US, "%.2f", analysis.notePercents.getValue(2))};
                |"x4Percent": ${String.format(Locale.US, "%.2f", analysis.notePercents.getValue(4))};
                |"x8Percent": ${String.format(Locale.US, "%.2f", analysis.notePercents.getValue(8))}
             |}""".trimMargin()
}

fun serializeAnalysisTask(task: AnalysisTask): String {
    val type = when (task) {
        is AnalysisTask.Ready -> "ready"
        is AnalysisTask.Processing -> "processing"
        is AnalysisTask.Finished -> "finished"
        is AnalysisTask.Failed -> "failed"
    }
    val data = when (task) {
        is AnalysisTask.Ready -> """{ "localPath": "${task.localPath}" }"""
        is AnalysisTask.Processing ->
            """{ "processingPercent": ${String.format(Locale.US, "%.2f", task.processingPercent)} }"""
        is AnalysisTask.Finished -> serializeFulfilledAnalysis(task.analysis)
        is AnalysisTask.Failed -> """{}"""
    }
    return """{
                |"type": "$type";
                |"trackId": ${task.trackId};
                |"data": $data
             |}""".trimMargin()
}

fun serializeAnalysisListUpdate(update: AnalysisListUpdate): String {
    val type = when (update) {
        is AnalysisListUpdate.AddItem -> "add"
        is AnalysisListUpdate.RemoteItem -> "remove"
        is AnalysisListUpdate.ChangeItem -> "change"
        is AnalysisListUpdate.NoChange -> "no_change"
    }
    val data = when (update) {
        is AnalysisListUpdate.AddItem -> """{ "newItem": ${serializeAnalysisTask(update.task)} }"""
        is AnalysisListUpdate.RemoteItem -> """{ "oldItem": ${serializeAnalysisTask(update.task)} }"""
        is AnalysisListUpdate.ChangeItem -> """{
                                                 |"oldItem": ${serializeAnalysisTask(update.oldTask)};
                                                 |"newItem": ${serializeAnalysisTask(update.newTask)}
                                              |}""".trimMargin()
        is AnalysisListUpdate.NoChange -> """{}"""
    }
    return """{
                |"type": "$type";
                |"data": $data
             |}""".trimMargin()
}