package com.appspot.magtech.application

import com.appspot.magtech.application.analysis.AnalysisAction
import com.appspot.magtech.application.analysis.AnalysisState
import com.appspot.magtech.application.analysis.analysisReducer
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import org.reduxkotlin.Reducer
import org.reduxkotlin.Store
import org.reduxkotlin.middleware

@ObsoleteCoroutinesApi
val ReduxContext = newSingleThreadContext("ReduxContext")

interface Action

inline fun <S, reified A> middlewareForAction(
    crossinline actionMiddleware: (store: Store<S>, action: A) -> Action
) = middleware<S> { store, next, action ->
    val result = when (action) {
        is A -> actionMiddleware(store, action)
        else -> action
    }
    next(result)
}

data class AppState(
    val analysisState: AnalysisState = AnalysisState()
)

val appReducer: Reducer<AppState> = { state, action ->
    when (action) {
        is AnalysisAction -> state.copy(analysisState = analysisReducer(state.analysisState, action))
        else -> state
    }
}