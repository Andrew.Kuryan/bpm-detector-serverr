package com.appspot.magtech.application

import com.appspot.magtech.api.db.TracksApi
import com.appspot.magtech.application.analysis.addTrackMiddleware
import com.appspot.magtech.application.analysis.completeTaskMiddleware
import com.appspot.magtech.application.analysis.loadAnalysisMiddleware
import com.appspot.magtech.application.analysis.removeAnalysisMiddleware
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import org.reduxkotlin.applyMiddleware
import org.reduxkotlin.createThreadSafeStore

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
fun configureStore(api: TracksApi): Pair<Flow<AppState>, Channel<Action>> {
    val store = createThreadSafeStore(
        appReducer,
        AppState(),
        applyMiddleware(
            addTrackMiddleware,
            completeTaskMiddleware(api),
            loadAnalysisMiddleware(api),
            removeAnalysisMiddleware(api),
        )
    )

    val states = callbackFlow<AppState> {
        val unsubscribe = store.subscribe {
            trySendBlocking(store.state)
        }
        send(store.state)

        awaitClose { unsubscribe() }
    }

    val actions = Channel<Action>()
    CoroutineScope(ReduxContext).launch {
        actions.receiveAsFlow().collect {
            store.dispatch(it)
        }
    }

    return states to actions
}