package com.appspot.magtech.application.analysis

import com.appspot.magtech.DetectorConfig
import com.appspot.magtech.MP3AudioFile
import com.appspot.magtech.api.db.TracksApi
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.ReduxContext
import com.appspot.magtech.application.middlewareForAction
import com.appspot.magtech.entities.FulfilledAnalysis
import com.appspot.magtech.processFile
import kotlinx.coroutines.*
import java.nio.file.Paths
import kotlin.math.roundToInt

@ObsoleteCoroutinesApi
val loadAnalysisMiddleware = { api: TracksApi ->
    middlewareForAction<AppState, AnalysisAction.LoadAnalysis> { store, action ->
        CoroutineScope(ReduxContext).launch {
            try {
                val result = withContext(Dispatchers.IO) { api.getAllTrackAnalysis() }
                store.dispatch(AnalysisAction.LoadAnalysisSuccess(result))
            } catch (exc: Exception) {
                println(exc)
            }
        }
        action
    }
}

@ObsoleteCoroutinesApi
val removeAnalysisMiddleware = { api: TracksApi ->
    middlewareForAction<AppState, AnalysisAction.RemoveAnalysis> { store, action ->
        CoroutineScope(ReduxContext).launch {
            try {
                withContext(Dispatchers.IO) { api.removeTrackAnalysis(action.trackId) }
                store.dispatch(AnalysisAction.RemoveAnalysisSuccess(action.trackId))
            } catch (exc: Exception) {
                println(exc)
            }
        }
        action
    }
}

@ObsoleteCoroutinesApi
val addTrackMiddleware =
    middlewareForAction<AppState, AnalysisAction.TaskAdded> { store, action ->
        CoroutineScope(Dispatchers.Default).launch {
            println("Task started: ${action.trackId} ${action.localPath}")
            try {
                withContext(ReduxContext) {
                    store.dispatch(AnalysisAction.ProgressUpdated(action.trackId, 0.0, action.hash))
                }
                val processResult = processFile(
                    MP3AudioFile(Paths.get(action.localPath)),
                    DetectorConfig(),
                ) { newPercent ->
                    withContext(ReduxContext) {
                        store.dispatch(AnalysisAction.ProgressUpdated(action.trackId, newPercent, action.hash))
                    }
                }
                val fulfilledAnalysis = FulfilledAnalysis(
                    processResult.bpmMin.roundToInt(),
                    processResult.tonality.toString(),
                    processResult.notePercents,
                )
                withContext(ReduxContext) {
                    store.dispatch(AnalysisAction.TaskCompleted(action.trackId, fulfilledAnalysis, action.hash))
                }
            } catch (exc: Exception) {
                println(exc)
                withContext(ReduxContext) { store.dispatch(AnalysisAction.TaskFailed(action.trackId, action.hash)) }
            }
            println("Task completed: ${action.trackId}")
        }
        action
    }

val completeTaskMiddleware = { api: TracksApi ->
    middlewareForAction<AppState, AnalysisAction.TaskCompleted> { store, action ->
        if (store.state.analysisState.processingTracks.find { it.hash == action.hash } != null) {
            CoroutineScope(Dispatchers.IO).launch {
                try {
                    api.createTrackAnalysis(action.trackId, action.analysis)
                } catch (exc: Exception) {
                    println(exc)
                }
            }
        }
        action
    }
}