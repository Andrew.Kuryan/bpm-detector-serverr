package com.appspot.magtech.application.analysis

import com.appspot.magtech.application.Action
import com.appspot.magtech.entities.FulfilledAnalysis
import org.reduxkotlin.ReducerForActionType

object HashGenerator {

    private var currentHash = 0
    val nextValue: Int
        get() {
            currentHash += 1
            return currentHash
        }
}

sealed class AnalysisAction : Action {
    object LoadAnalysis : AnalysisAction()
    data class LoadAnalysisSuccess(val analysis: List<Pair<Int, FulfilledAnalysis>>) : AnalysisAction()

    data class RemoveAnalysis(val trackId: Int) : AnalysisAction()
    data class RemoveAnalysisSuccess(val trackId: Int) : AnalysisAction()

    data class TaskAdded(val trackId: Int, val localPath: String, val hash: Int) : AnalysisAction()
    data class ProgressUpdated(val trackId: Int, val newPercent: Double, val hash: Int) : AnalysisAction()
    data class TaskCompleted(val trackId: Int, val analysis: FulfilledAnalysis, val hash: Int) : AnalysisAction()
    data class TaskFailed(val trackId: Int, val hash: Int) : AnalysisAction()
}

data class AnalysisState(
    val processingTracks: List<AnalysisTask> = listOf()
)

sealed class AnalysisTask {
    abstract val trackId: Int
    abstract val hash: Int

    data class Ready(override val trackId: Int, override val hash: Int, val localPath: String) : AnalysisTask()
    data class Processing(override val trackId: Int, override val hash: Int, val processingPercent: Double) :
        AnalysisTask()

    data class Finished(override val trackId: Int, override val hash: Int, val analysis: FulfilledAnalysis) :
        AnalysisTask()

    data class Failed(override val trackId: Int, override val hash: Int) : AnalysisTask()
}

val analysisReducer: ReducerForActionType<AnalysisState, AnalysisAction> = { state, action ->
    when (action) {
        is AnalysisAction.LoadAnalysis -> state
        is AnalysisAction.LoadAnalysisSuccess -> state.copy(
            processingTracks = action.analysis.map {
                AnalysisTask.Finished(
                    it.first,
                    HashGenerator.nextValue,
                    it.second
                )
            }
        )

        is AnalysisAction.RemoveAnalysis -> state
        is AnalysisAction.RemoveAnalysisSuccess -> state.copy(
            processingTracks = state.processingTracks.filter { it.trackId != action.trackId }
        )

        is AnalysisAction.TaskAdded -> state.copy(
            processingTracks = state.processingTracks + AnalysisTask.Ready(
                action.trackId,
                action.hash,
                action.localPath
            )
        )
        is AnalysisAction.ProgressUpdated -> state.copy(
            processingTracks = state.processingTracks.map {
                if (it.hash == action.hash) AnalysisTask.Processing(
                    action.trackId,
                    action.hash,
                    action.newPercent
                ) else it
            }
        )
        is AnalysisAction.TaskCompleted -> state.copy(
            processingTracks = state.processingTracks.map {
                if (it.hash == action.hash) AnalysisTask.Finished(
                    action.trackId,
                    action.hash,
                    action.analysis
                ) else it
            }
        )
        is AnalysisAction.TaskFailed -> state.copy(
            processingTracks = state.processingTracks.map {
                if (it.hash == action.hash) AnalysisTask.Failed(action.trackId, action.hash) else it
            }
        )
    }
}