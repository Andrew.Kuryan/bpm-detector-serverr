package com.appspot.magtech.bpm

import com.appspot.magtech.logic.Constants
import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.roundToInt

data class DetectorResult(
    val bpm: Double, val deviation: Double, val index: Int, val variantNumber: Int,
    val initialSizeMap: Map<Int, Int> = mapOf(), val sizeMap: Map<Int, Int> = mapOf(),
)

class BlockNotesDetector(val sampleRate: Float, val windowSize: Int) {
    val prevNoteIndexes = mutableMapOf<Int, Int>()
    val sizeMap = mutableMapOf<Int, Int>()
}

fun BlockNotesDetector.nextBlock(notes: List<Int>) {
    prevNoteIndexes.keys.filter { it !in notes }.forEach { noteIndex ->
        val count = prevNoteIndexes.getValue(noteIndex)
        if (sizeMap[count] == null) {
            sizeMap[count] = 1
        } else {
            sizeMap[count] = sizeMap.getValue(count) + 1
        }
        prevNoteIndexes.remove(noteIndex)
    }
    notes.forEach { noteIndex ->
        if (prevNoteIndexes[noteIndex] == null) {
            prevNoteIndexes[noteIndex] = 1
        } else {
            prevNoteIndexes[noteIndex] = prevNoteIndexes.getValue(noteIndex) + 1
        }
    }
}

fun BlockNotesDetector.finishCount(): DetectorResult {
    val variant1 = pickUpVariant(
        Constants.variant1Probs,
        sizeMap,
    )
    val variant2 = pickUpVariant(
        Constants.variant2Probs,
        sizeMap,
    )

    val (variant, variantNumber) = if (variant1.deviation > variant2.deviation) {
        variant2 to 2
    } else {
        variant1 to 1
    }

    return DetectorResult(
        60.0 / (variant.index * 2 * (windowSize.toDouble() / sampleRate)),
        variant.deviation,
        variant.index,
        variantNumber,
        variant.initialSizeMap,
        variant.sizeMap,
    )
}

data class PickUpResult(
    val index: Int,
    val deviation: Double,
    val sizeMap: Map<Int, Int>,
    val initialSizeMap: Map<Int, Int>,
    val notesCount: Int,
    val minKey: Int,
    val maxKey: Int,
)

fun pickUpVariant(variantProbs: List<Double>, sizeMap: Map<Int, Int>): PickUpResult {
    return (11..50).map { i ->
        val combinedMap = sizeMap.toMutableMap()

        (0..3).windowed(2).forEach { (start, end) ->
            val mapStart = (i * 2.0.pow(start)).roundToInt()
            val mapEnd = (i * 2.0.pow(end)).roundToInt()

            val currentStartNum = combinedMap[mapStart] ?: 0
            val currentEndNum = combinedMap[mapEnd] ?: 0

            combinedMap[mapStart] = currentStartNum + ((mapStart + 1)..((mapStart + mapEnd) / 2)).map {
                val prev = combinedMap[it] ?: 0
                combinedMap[it] = 0
                prev
            }.sum()

            combinedMap[mapEnd] = currentEndNum + ((((mapStart + mapEnd) / 2) + 1) until mapEnd).map {
                val prev = combinedMap[it] ?: 0
                combinedMap[it] = 0
                prev
            }.sum()
        }

        val mapPrev = i / 2
        val mapStart = i
        combinedMap[mapStart] = combinedMap.getValue(mapStart) + ((((mapPrev + mapStart) / 2) + 1) until mapStart).map {
            val prev = combinedMap[it] ?: 0
            combinedMap[it] = 0
            prev
        }.sum()

        val mapNext = i * 16
        val mapEnd = i * 8
        combinedMap[mapEnd] = combinedMap.getValue(mapEnd) + ((mapEnd + 1)..((mapEnd + mapNext) / 2)).map {
            val prev = combinedMap[it] ?: 0
            combinedMap[it] = 0
            prev
        }.sum()

        val notesCount = combinedMap.filter {
            it.key >= (((mapPrev + mapStart) / 2) + 1) && it.key <= ((mapEnd + mapNext) / 2)
        }.values.sum()

        PickUpResult(
            i, (0..3).sumByDouble { j ->
                abs(
                    combinedMap.getValue((i * 2.0.pow(j)).roundToInt()).toDouble() / notesCount -
                            variantProbs[j]
                ) / variantProbs[j]
            }, combinedMap, sizeMap, notesCount,
            ((mapPrev + mapStart) / 2) + 1,
            (mapEnd + mapNext) / 2
        )
    }.minByOrNull { it.deviation }!!
}