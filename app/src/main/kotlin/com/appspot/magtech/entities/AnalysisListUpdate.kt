package com.appspot.magtech.entities

import com.appspot.magtech.application.analysis.AnalysisTask

sealed class AnalysisListUpdate {
    data class AddItem(val task: AnalysisTask) : AnalysisListUpdate()
    data class ChangeItem(val oldTask: AnalysisTask, val newTask: AnalysisTask) : AnalysisListUpdate()
    data class RemoteItem(val task: AnalysisTask) : AnalysisListUpdate()
    object NoChange : AnalysisListUpdate()
}

fun computeDifference(oldList: List<AnalysisTask>, newList: List<AnalysisTask>): AnalysisListUpdate {
    if (oldList.size == newList.size) {
        for (oldListTask in oldList) {
            val newListTask =
                newList.find { it.trackId == oldListTask.trackId } ?: throw Exception("Unacceptable change")
            if (newListTask != oldListTask) {
                return AnalysisListUpdate.ChangeItem(oldListTask, newListTask)
            }
        }
    } else {
        if (oldList.size > newList.size) {
            for (oldListTask in oldList) {
                newList.find { it == oldListTask } ?: return AnalysisListUpdate.RemoteItem(oldListTask)
            }
        } else if (newList.size > oldList.size) {
            for (newListTask in newList) {
                oldList.find { it == newListTask } ?: return AnalysisListUpdate.AddItem(newListTask)
            }
        }
    }
    return AnalysisListUpdate.NoChange
}