package com.appspot.magtech.entities

sealed class TrackAnalysis

object NotAvailableAnalysis : TrackAnalysis()

data class ProcessingAnalysis(val percent: Double) : TrackAnalysis()

data class FulfilledAnalysis(
    val bpm: Int,
    val tempo: Tempo,
    val tonality: Tonality,
    val notePercents: Map<Int, Double>
) : TrackAnalysis() {

    constructor(bpm: Int, key: String, notePercents: Map<Int, Double>) : this(
        bpm,
        getTempoFromBPM(bpm),
        Tonality(Mode.valueOf(key.split(" ")[1]), Note.valueOf(key.split(" ")[0])),
        notePercents,
    )
}

data class Tonality(val mode: Mode, val note: Note) {

    override fun toString() = "${note.name} ${mode.name}"
}

enum class Mode {
    Major, Minor,
}

enum class Note {
    C, `C#`, D, `D#`, E, F, `F#`, G, `G#`, A, `A#`, B,
}

enum class Tempo {
    ANDANTE, ANDANTINO, ALLEGRETTO, ANIMATO, ALLEGRO, VIVO, PRESTO, PRESTISSIMO, UNKNOWN;

    companion object {
        val available = listOf(ANDANTE, ANDANTINO, ALLEGRETTO, ANIMATO, ALLEGRO, VIVO, PRESTO, PRESTISSIMO)
    }
}

fun getTempoFromBPM(bpm: Int): Tempo {
    return when (bpm) {
        in 50..72 -> Tempo.ANDANTE
        in 73..90 -> Tempo.ANDANTINO
        in 91..108 -> Tempo.ALLEGRETTO
        in 109..120 -> Tempo.ANIMATO
        in 121..154 -> Tempo.ALLEGRO
        in 155..183 -> Tempo.VIVO
        in 184..200 -> Tempo.PRESTO
        in 201..300 -> Tempo.PRESTISSIMO
        else -> Tempo.UNKNOWN
    }
}