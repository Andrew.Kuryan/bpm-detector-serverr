package com.appspot.magtech.extensions.flow

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.transform

@ExperimentalCoroutinesApi
val <T> Flow<T>.zipLast: (Int) -> Flow<List<T?>>
    get() = { n ->
        val buffer: MutableList<T?> = (1..n).map { null }.toMutableList()
        transform {
            for (i in 1 until n) {
                buffer[i] = buffer[i - 1]
            }
            buffer[0] = it
            emit(buffer.toList())
        }
    }