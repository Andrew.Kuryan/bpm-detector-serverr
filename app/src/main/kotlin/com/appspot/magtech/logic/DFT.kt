package com.appspot.magtech.logic

import org.apache.commons.math3.transform.TransformType
import kotlin.math.*

fun STFT(source: DoubleArray, n: Int, m: Int): List<Double> {
    val N = 2 * m
    val rangeStart = max(n - m, 0)

    val windowCoeffs = Constants.blackmannNattalCoefficients.getValue(m)
    val part = DoubleArray(N)
    System.arraycopy(source, rangeStart, part, 0, N)
    for (i in 0 until N) {
        part[i] = windowCoeffs[i] * part[i]
    }

    return Constants.fftTransformer.transform(part, TransformType.FORWARD)
        .map { sqrt(it.real.pow(2) + it.imaginary.pow(2)) }
}

fun blackmanNattalWindow(n: Int, N: Int): Double {
    return 0.3635819 -
            0.4891775 * cos(2 * PI * n / (N - 1)) +
            0.1365995 * cos(4 * PI * n / (N - 1)) -
            0.0106411 * cos(6 * PI * n / (N - 1))
}