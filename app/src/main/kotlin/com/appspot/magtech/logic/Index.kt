package com.appspot.magtech.logic

import org.apache.commons.math3.transform.DftNormalization
import org.apache.commons.math3.transform.FastFourierTransformer
import kotlin.math.exp
import kotlin.math.pow
import kotlin.math.sqrt

object Constants {
    val fftTransformer = FastFourierTransformer(DftNormalization.STANDARD)

    val blackmannNattalCoefficients = mapOf(
        (32768 / 2) to (0 until 32768).map { blackmanNattalWindow(it, 32768) }.toList(),
        (512 / 2) to (0 until 512).map { blackmanNattalWindow(it, 512) }.toList(),
    )

    private val harmonicsDivider = (0..5).map { exp(-it.toDouble()) }.sum()
    val harmomicCoefficients = (0..5).map { exp(-it.toDouble()) / harmonicsDivider }
    private val harmonicsMedian = harmomicCoefficients.average()
    val harmonicsMultipliers = harmomicCoefficients.map { it - harmonicsMedian }

    val variant1Probs = listOf(0.6, 0.25, 0.1, 0.05)
    val variant2Probs = listOf(0.05, 0.3, 0.45, 0.2)
}

fun correlationCoefficient(source1: List<Double>, source2: List<Double>): Double {
    val m1 = source1.average()
    val m2 = source2.average()
    val d1 = 1.0 / (source1.size - 1) * source1.sumByDouble { (it - m1).pow(2) }
    val d2 = 1.0 / (source2.size - 1) * source2.sumByDouble { (it - m2).pow(2) }

    val m = 1.0 / (source1.size - 1) *
            (source1.indices).sumByDouble { i -> (source1[i] - m1) * (source2[i] - m2) }
    return m / sqrt(d1 * d2)
}
