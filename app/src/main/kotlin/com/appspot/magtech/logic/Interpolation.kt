package com.appspot.magtech.logic

fun getLagrangeInterpolation(
    xa1: Double, ya1: Double, xa2: Double, ya2: Double,
    xb1: Double, yb1: Double, xb2: Double, yb2: Double,
): (Double) -> Double {
    val l0Divider = (xa1 - xa2) * (xa1 - xb1) * (xa1 - xb2)
    val l0 = { x: Double -> (x - xa2) * (x - xb1) * (x - xb2) }

    val l1Divider = (xa2 - xa1) * (xa2 - xb1) * (xa2 - xb2)
    val l1 = { x: Double -> (x - xa1) * (x - xb1) * (x - xb2) }

    val l2Divider = (xb1 - xa1) * (xb1 - xa2) * (xb1 - xb2)
    val l2 = { x: Double -> (x - xa1) * (x - xa2) * (x - xb2) }

    val l3Divider = (xb2 - xa1) * (xb2 - xa2) * (xb2 - xb1)
    val l3 = { x: Double -> (x - xa1) * (x - xa2) * (x - xb1) }

    val l0Mult = ya1 / l0Divider
    val l1Mult = ya2 / l1Divider
    val l2Mult = yb1 / l2Divider
    val l3Mult = yb2 / l3Divider

    return { x: Double -> l0Mult * l0(x) + l1Mult * l1(x) + l2Mult * l2(x) + l3Mult * l3(x) }
}

fun interpolateSource(source: List<Double>, targetSize: Int): List<Double> {
    val innerPoints = targetSize / source.size - 1
    val result = MutableList(targetSize) { 0.0 }
    val values = source.mapIndexed { index, d -> index * (innerPoints + 1) to d } +
            Pair(source.size * (innerPoints + 1), 0.0)

    var leftPrevEntry = Pair(-(innerPoints + 1), 0.0)
    var leftEntry = values[0]

    for (i in 1 until values.size - 1) {
        val rightEntry = values[i]
        val rightNextEntry = values[i + 1]

        result[leftEntry.first] = leftEntry.second
        val interpolation = getLagrangeInterpolation(
            leftPrevEntry.first.toDouble(), leftPrevEntry.second, leftEntry.first.toDouble(), leftEntry.second,
            rightNextEntry.first.toDouble(), rightNextEntry.second, rightEntry.first.toDouble(), rightEntry.second,
        )
        for (j in 1..innerPoints) {
            val freq = leftEntry.first + j
            result[freq] = interpolation(freq.toDouble())
        }

        leftEntry = values[i]
        leftPrevEntry = values[i - 1]
    }

    return result
}