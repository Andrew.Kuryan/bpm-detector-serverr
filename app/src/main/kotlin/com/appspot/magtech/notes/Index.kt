package com.appspot.magtech.notes

import com.appspot.magtech.logic.Constants
import kotlin.math.roundToInt

fun calcNoteFrequencies(source: List<Double>, freqStep: Double): Map<String, Double> {
    val result = mutableMapOf<String, Double>()
    for (octave in 0..8) {
        for ((i, note) in Note.bases.withIndex()) {
            val dpfIndex = (note(octave)!!.first / freqStep).roundToInt()
            val prevDpfIndex = ((note.prev(octave)?.first ?: 15.0) / freqStep).roundToInt()
            val nextDpfIndex = ((note.next(octave)?.first ?: 8000.0) / freqStep).roundToInt()

            val startIndex = (dpfIndex * 0.9 + prevDpfIndex * 0.1).roundToInt()
            val endIndex = (dpfIndex * 0.9 + nextDpfIndex * 0.1).roundToInt()

            var max = 0.0
            // var sum = 0.0
            for (k in startIndex..endIndex) {
                if (source[k] > max) {
                    max = source[k]
                }
            }
            result["${note.displayName}$octave"] = max
        }
    }
    return result
}

fun calcNotes(source: List<Double>, freqStep: Double): List<Double> {
    val noteFrequencies = calcNoteFrequencies(source, freqStep)
    val notesMedian = noteFrequencies.values.average()

    return (0..8).map { octave ->
        Note.bases.map { note ->
            (listOf(note(octave)) + Note.overtones.getValue(note)(octave)).mapIndexed { index, freqOctave ->
                (if (freqOctave == null) 0.0 else noteFrequencies["${freqOctave.third.displayName}${freqOctave.second}"]
                    ?: 0.0 - notesMedian) *
                        Constants.harmonicsMultipliers[index]
            }.sum()
        }
    }.flatten()
}