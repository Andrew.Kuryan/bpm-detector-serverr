package com.appspot.magtech.notes

data class Note(val displayName: String, private val frequencies: List<Double>) {

    operator fun invoke(octave: Int): Triple<Double, Int, Note>? {
        return if (frequencies.getOrNull(octave) != null) Triple(frequencies[octave], octave, this) else null
    }

    fun prev(octave: Int): Triple<Double, Int, Note>? {
        return if (bases.indexOf(this) - 1 < 0) {
            if (bases.last()(octave - 1) != null) bases.last()(octave - 1) else null
        } else {
            bases[bases.indexOf(this) - 1](octave)
        }
    }

    fun next(octave: Int): Triple<Double, Int, Note>? {
        return if (bases.indexOf(this) + 1 >= bases.size) {
            if (bases.first()(octave + 1) != null) bases.first()(octave + 1) else null
        } else {
            bases[bases.indexOf(this) + 1](octave)
        }
    }

    companion object {
        val C = Note(
            "C",
            listOf(16.35160, 32.7032, 65.4064, 130.8128, 261.6256, 523.2512, 1046.5024, 2093.0048, 4186.0096)
        )
        val `C#` = Note(
            "C#",
            listOf(17.32391, 34.6479, 69.2958, 138.5916, 277.1832, 554.3664, 1108.7328, 2217.4656, 4434.922)
        )
        val D = Note(
            "D",
            listOf(18.35405, 36.7081, 73.4162, 146.8324, 293.6648, 587.3296, 1174.6592, 2349.3184, 4698.636)
        )
        val `D#` = Note(
            "D#",
            listOf(19.44544, 38.8909, 77.7818, 155.5636, 311.1272, 622.2544, 1244.5088, 2489.0176, 4978.032)
        )
        val E = Note(
            "E",
            listOf(20.60172, 41.2035, 82.4070, 164.8140, 329.6280, 659.2560, 1318.5120, 2637.0240, 5274.041)
        )
        val F = Note(
            "F",
            listOf(21.82676, 43.6536, 87.3072, 174.6144, 349.2288, 698.4576, 1396.9152, 2793.8304, 5587.652)
        )
        val `F#` = Note(
            "F#",
            listOf(23.12465, 46.2493, 92.4986, 184.9972, 369.9944, 739.9888, 1479.9776, 2959.9552, 5919.911)
        )
        val G = Note(
            "G",
            listOf(24.49971, 48.9995, 97.9990, 195.9980, 391.9960, 783.9920, 1567.9840, 3135.9680, 6271.927)
        )
        val `G#` = Note(
            "G#",
            listOf(25.95654, 51.9130, 103.8260, 207.6520, 415.3040, 830.6080, 1661.2160, 3322.4320, 6644.875)
        )
        val A = Note(
            "A",
            listOf(27.5000, 55.0000, 110.0000, 220.0000, 440.0000, 880.0000, 1760.0000, 3520.0000, 7040.0000)
        )
        val `A#` = Note(
            "A#",
            listOf(29.1353, 58.2706, 116.5412, 233.0824, 466.1648, 932.3296, 1864.6592, 3729.3184, 7458.620)
        )
        val B = Note(
            "B",
            listOf(30.8677, 61.7354, 123.4708, 246.9416, 493.8832, 987.7664, 1975.5328, 3951.0656, 7902.133)
        )

        val bases = listOf(C, `C#`, D, `D#`, E, F, `F#`, G, `G#`, A, `A#`, B)

        val overtones = mapOf<Note, (Int) -> List<Triple<Double, Int, Note>?>>(
            C to {
                listOf(C(it + 1), G(it + 1), C(it + 2), E(it + 2), G(it + 2))
            },
            `C#` to {
                listOf(`C#`(it + 1), `G#`(it + 1), `C#`(it + 2), F(it + 2), `G#`(it + 2))
            },
            D to {
                listOf(D(it + 1), A(it + 1), D(it + 2), `F#`(it + 2), A(it + 2))
            },
            `D#` to {
                listOf(`D#`(it + 1), `A#`(it + 1), `D#`(it + 2), G(it + 2), `A#`(it + 2))
            },
            E to {
                listOf(E(it + 1), B(it + 1), E(it + 2), `G#`(it + 2), B(it + 2))
            },
            F to {
                listOf(F(it + 1), C(it + 2), F(it + 2), A(it + 2), C(it + 3))
            },
            `F#` to {
                listOf(`F#`(it + 1), `C#`(it + 2), `F#`(it + 2), `A#`(it + 2), `C#`(it + 3))
            },
            G to {
                listOf(G(it + 1), D(it + 2), G(it + 2), B(it + 2), D(it + 3))
            },
            `G#` to {
                listOf(`G#`(it + 1), `D#`(it + 2), `G#`(it + 2), C(it + 3), `D#`(it + 3))
            },
            A to {
                listOf(A(it + 1), E(it + 2), A(it + 2), `C#`(it + 3), E(it + 3))
            },
            `A#` to {
                listOf(`A#`(it + 1), F(it + 2), `A#`(it + 2), D(it + 3), F(it + 3))
            },
            B to {
                listOf(B(it + 1), `F#`(it + 2), B(it + 2), `D#`(it + 3), `F#`(it + 3))
            }
        )
    }
}