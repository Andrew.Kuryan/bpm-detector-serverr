package com.appspot.magtech

import com.appspot.magtech.api.parsers.AnalysisListUpdate
import com.appspot.magtech.api.parsers.computeDifference
import com.appspot.magtech.application.analysis.AnalysisTask
import com.appspot.magtech.entities.FulfilledAnalysis
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class AnalysisListUpdateTest {

    @Test
    fun testChangeDifference1() {
        val oldList = listOf(
            AnalysisTask.Ready(1, "path_1"),
            AnalysisTask.Processing(2, 0.3),
            AnalysisTask.Processing(3, 0.6),
            AnalysisTask.Finished(
                4,
                FulfilledAnalysis(100, "A Minor", mapOf(1 to 0.5, 2 to 0.3, 4 to 0.1, 8 to 0.1))
            ),
        )
        val newList = listOf(
            AnalysisTask.Ready(1, "path_1"),
            AnalysisTask.Processing(2, 0.4),
            AnalysisTask.Processing(3, 0.6),
            AnalysisTask.Finished(
                4,
                FulfilledAnalysis(100, "A Minor", mapOf(1 to 0.5, 2 to 0.3, 4 to 0.1, 8 to 0.1))
            ),
        )
        val result = computeDifference(oldList, newList)
        assertTrue(result is AnalysisListUpdate.ChangeItem)
        result as AnalysisListUpdate.ChangeItem
        assertTrue(result.oldTask is AnalysisTask.Processing)
        assertTrue(result.newTask is AnalysisTask.Processing)
        assertTrue((result.oldTask as AnalysisTask.Processing).processingPercent == 0.3)
        assertTrue((result.newTask as AnalysisTask.Processing).processingPercent == 0.4)
    }

    @Test
    fun testChangeDifference2() {
        val oldList = listOf(
            AnalysisTask.Ready(1, "path_1"),
        )
        val newList = listOf(
            AnalysisTask.Processing(1, 0.0),
        )
        val result = computeDifference(oldList, newList)
        assertTrue(result is AnalysisListUpdate.ChangeItem)
        result as AnalysisListUpdate.ChangeItem
        assertTrue(result.newTask is AnalysisTask.Processing)
    }

    @Test
    fun testRemoveDifference() {
        val oldList = listOf(
            AnalysisTask.Ready(1, "path_1"),
            AnalysisTask.Processing(2, 0.3),
            AnalysisTask.Processing(3, 0.6),
            AnalysisTask.Finished(
                4,
                FulfilledAnalysis(100, "A Minor", mapOf(1 to 0.5, 2 to 0.3, 4 to 0.1, 8 to 0.1))
            ),
        )
        val newList = listOf(
            AnalysisTask.Ready(1, "path_1"),
            AnalysisTask.Processing(2, 0.3),
            AnalysisTask.Processing(3, 0.6),
        )
        val result = computeDifference(oldList, newList)
        assertTrue(result is AnalysisListUpdate.RemoteItem)
        result as AnalysisListUpdate.RemoteItem
        assertTrue(result.task is AnalysisTask.Finished)
        assertTrue((result.task as AnalysisTask.Finished).trackId == 4)
    }

    @Test
    fun testAddDifference() {
        val oldList = listOf(
            AnalysisTask.Processing(2, 0.3),
            AnalysisTask.Processing(3, 0.6),
            AnalysisTask.Finished(
                4,
                FulfilledAnalysis(100, "A Minor", mapOf(1 to 0.5, 2 to 0.3, 4 to 0.1, 8 to 0.1))
            ),
        )
        val newList = listOf(
            AnalysisTask.Ready(1, "path_1"),
            AnalysisTask.Processing(2, 0.3),
            AnalysisTask.Processing(3, 0.6),
            AnalysisTask.Finished(
                4,
                FulfilledAnalysis(100, "A Minor", mapOf(1 to 0.5, 2 to 0.3, 4 to 0.1, 8 to 0.1))
            ),
        )
        val result = computeDifference(oldList, newList)
        assertTrue(result is AnalysisListUpdate.AddItem)
        result as AnalysisListUpdate.AddItem
        assertTrue(result.task is AnalysisTask.Ready)
        assertTrue((result.task as AnalysisTask.Ready).trackId == 1)
    }
}